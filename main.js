
let controllers = [];

let w = 800;
let h = 600;

const levelSetup = {
    jugadores: [
        {id: 0, x: 60, y: 80, w: 20, h: 50, c: "#3498db"},
        //{id: 1, x: 650, y: 300, w: 20, h: 50, c: "#8e44ad"}
    ],
    plataformas: [
        {x: 0, y: 0, w: w, h: 20},
        {x: 0, y: h - 20, w: w, h: 20},
        {x: 0, y: 0, w: 20, h: h},
        {x: w - 20, y: 0, w: 20, h: h},
        {x: 50, y: 350, w: 200, h: 10},
        {x: 550, y: 350, w: 200, h: 10},
        {x: 300, y: 500, w: 200, h: 10},
        {x: 100, y: 550, w: 100, h: 10},
        {x: 600, y: 550, w: 100, h: 10},
        {x: 370, y: 420, w: 100, h: 10},
        {x: 450, y: 280, w: 100, h: 10}
    ],
    monedas: [
        {x: 20, y: 20, w: 200, h: 100}
    ],
    trampas: [
        {x: 350, y: 480, w: 100, h: 20}
    ]
};

let plataformas = [];
let jugadores = [];
let monedas = [];
let trampas = [];
let pltCercanas = [];



function setup() {
    createCanvas(w, h);

    // GAMEPAD SETUP
    window.addEventListener("gamepadconnected", function(e) {
    gamepadHandler(e, true);
    console.log("Gamepad connected at index %d: %s. %d buttons, %d axes.",
        e.gamepad.index, e.gamepad.id,
        e.gamepad.buttons.length, e.gamepad.axes.length);      
    });
        window.addEventListener("gamepaddisconnected", function(e) {
    console.log("Gamepad disconnected from index %d: %s",
        e.gamepad.index, e.gamepad.id);      
        colour=color(120,0,0)
        gamepadHandler(e, false);
    });   
    setupGameLevel();
}

function setupGameLevel() {
    levelSetup.plataformas.forEach(elem => {
        plataformas.push(new Plataforma(elem.x, elem.y, elem.w, elem.h));
    });
    levelSetup.jugadores.forEach(j => {
        jugadores.push(new Jugador(j.id, j.x, j.y, j.w, j.h, j.c));
    });
    levelSetup.trampas.forEach(t => {
        trampas.push(new Trampa(t.x, t.y, t.w, t.h));
    });
}

function draw() {
    background("#d7dbdd");

    updateGame();

    plataformas.forEach(elem => {
        elem.show();
    });
    trampas.forEach(elem => {
        elem.show();
    });
    jugadores.forEach(elem => {
        elem.show();
    });
}

function updateGame() {
    gestionarGamepad();
    jugadores.forEach(jugador => {
        jugador.update();
    });

    plataformas.forEach(plt => {
        jugadores[0].plataformDetection(plt);
    });

    plataformas.forEach(plt => {
        jugadores[0].resolvColision(plt);
    });
}

function gamepadHandler(event, connecting) {
    let gamepad = event.gamepad;
    if (connecting) {
            print("Connecting to controller "+gamepad.index)
        controllers[gamepad.index] = gamepad
    } else {
        delete controllers[gamepad.index]
    }
}

function gestionarGamepad() {
    var gamepads = navigator.getGamepads()
  
    for (let i in controllers) {
        push()
        let controller = gamepads[i]//controllers[i]
        if (controller.buttons) {
            for (let btn=0; btn<controller.buttons.length; btn++) {
                let val = controller.buttons[btn]
                console.log(val);
                strokeWeight(2)
                stroke('white')
                if (buttonPressed(val)) {
                    fill('green')
                    stroke('white')
                }else {
                    fill('red')
                    stroke('grey')
                }
            }
        }
        if (controller.axes) {
            let axes = controller.axes
            console.log(axes);
            for (let axis = 0; axis<axes.length; axis++) {
                let val = controller.axes[axis]
                fill('grey')
                stroke('white')
                noStroke()
                fill('yellow')
                fill('white')
            }
        }
        pop()
    }
}

function buttonPressed(b) {
    if (typeof(b) == "object") {
        return b.pressed; // binary 
    }
    return b > 0.9; // analog value
}

function keyPressed() {
    if(key == "w") {
        console.log("ley w pressed");
        jugadores[0].Keys.jump = true;
    }else if(key == "d") {
        jugadores[0].Keys.right = true;
    }else if(key == "s") {

    }else if(key == "a") {
        jugadores[0].Keys.left = true;
    }
}

function keyReleased() {
    if(key == "w") {
        console.log("ley w pressed");
        jugadores[0].Keys.jump = false;
    }else if(key == "d") {
        jugadores[0].Keys.right = false;
        jugadores[0].Keys.speed = 0;
    }else if(key == "s") {

    }else if(key == "a") {
        jugadores[0].Keys.left = false;
        jugadores[0].Keys.speed = 0;
    }
}

function keyTyped() {
    
}