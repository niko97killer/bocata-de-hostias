class Trampa extends Cosa{
    constructor(x,y,w,h){
        super(x,y,w,h)

    }

    show(){
        noStroke();
        fill(20,20,20)
        triangle(this.x, this.y + this.h, this.x + this.w / 3, this.y + this.h, this.x + this.w / 6, this.y);
        triangle(this.x + this.w / 3, this.y + this.h, this.x + this.w * 2 / 3, this.y + this.h, this.x + this.w / 2, this.y);
        triangle(this.x + this.w * 2 / 3, this.y + this.h, this.x + this.w, this.y + this.h, this.x + this.w * 5 / 6, this.y);

        let h2 = this.h/2;
        noStroke();
        fill(255,0,0);
        triangle(this.x + this.w / 12, this.y + h2, this.x+ this.w * 3 / 12, this.y + h2, this.x + this.w / 6, this.y);
        triangle(this.x + this.w * 5 / 12, this.y + h2, this.x + this.w * 7 / 12, this.y + h2, this.x + this.w / 2, this.y);
        triangle(this.x + this.w * 9 / 12, this.y + h2, this.x + this.w * 11 / 12, this.y + h2, this.x + this.w * 5 / 6, this.y);
    }
}