class Jugador extends Cosa {
    constructor(id, x, y, w, h, c) {
        super(x, y, w, h);
        this.id = id;
        this.color = c;
        this.monedas = 0;
        this.Keys = {
            jump: false,
            down: false,
            left: false,
            right: false
        }
        this.jumping = false;
        this.flor = false;
        this.jumCount = 17;
        this.jumpSpeed = 1;
        this.speed = 0;
        this.direction = 0;
    }

    

    show() {
        noStroke();
        fill(this.color);
        if(this.jumping) {
            rect(this.x, this.y + 10, this.w, this.h - 10);
        }else {
            rect(this.x, this.y, this.w, this.h);
        }
    }

    update() {
        if(!this.flor && !this.jumping) {
            this.y += this.jumpSpeed * 0.89;
            
            if(this.jumpSpeed < 10) {
                this.jumpSpeed += 1 * 0.5;
            }
        }

        if(this.Keys.left == true) {
            //this.x -= this.speed * 0.90;
            this.direction = 0.5;
            if(this.speed > -4) {
                this.speed -= 0.5;
            }
        }else if(this.Keys.right == true) {
            //this.x += this.speed * 0.90;
            this.direction = -0.5;
            if(this.speed < 4) {
                this.speed += 0.5;
            }
        }else {
            
            if(abs(this.speed) > 1) {
                this.speed += 1 * this.direction;
            }else {
                this.speed = 0;
            }
        }

        this.x += this.speed * 0.90;
        
        if(this.Keys.jump == true && !this.jumping && this.flor) {
            this.jumping = true;
            this.Keys.jump = false;
            
        }
        if(this.Keys.down == true) {

        }
        if(this.jumping) {
            this.y -= this.jumpSpeed * 0.9;
            this.jumpSpeed += 1 * 0.5;
            this.jumCount--;
            if(this.jumCount < 0) {
                this.jumCount = 17;
                this.jumping = false;
                this.direction = 0;
                this.jumpSpeed = 1;
            }
        }
        //this.Keys.jump = false;
    }

    plataformDetection(elem) {
        this.flor = false;
        //elem.colideSide[this.id] = '';
        if(this.x >= elem.x + elem.w/*&& this.y < elem.y + elem.h && this.y + this.h > elem.y*/) {
            elem.colideSide[this.id] = 'right';
        }else if(this.x + this.w <= elem.x /*&& this.y < elem.y + elem.h && this.y + this.h > elem.y*/) {
            elem.colideSide[this.id] = 'left';
        }else if(this.y >= elem.y + elem.h /*&& this.x < elem.x + elem.w && this.x + this.w > elem.x*/) {
            elem.colideSide[this.id] = 'bottom';
        }else if(this.y + this.h <= elem.y /*&& this.x < elem.x + elem.w && this.x + this.w > elem.x*/) {
            elem.colideSide[this.id] = 'top';
        }
    }

    resolvColision(elem) {
        if(this.x < elem.x + elem.w && this.x + this.w > elem.x
            && this.y < elem.y + elem.h && this.y + this.h > elem.y) {
                const colSide = elem.colideSide[this.id];
                if(colSide == "top") {
                    this.y = elem.y - this.h;
                    this.flor = true;
                    this.jumpSpeed = 1;
                }else if(colSide == "bottom") {
                    this.y = elem.y + elem.h;
                }else if(colSide == "right") {
                    this.speed = 0;
                    this.x = elem.x + elem.w;
                }else if(colSide == "left") {
                    this.speed = 0;
                    this.x = elem.x - this.w;
                }
        }
    }

    colosion(elem) {
        if(this.x > elem.x && this.x < elem.x + elem.w
            && this.y + this.w > elem.y && this.y < elem.y + elem.h) {
                if(elem instanceof Plataforma) {
                    this.flor = true;
                    this.y = elem.y + this.h;
                }else if(elem instanceof Moneda) {

                }else if(elem instanceof Trampa) {

                }else if(elem instanceof Proyectil) {
                    
                }else if(elem instanceof Jugador) {

                }
        }
    }
}