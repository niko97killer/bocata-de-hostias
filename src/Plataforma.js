class Plataforma extends Cosa{
    constructor(x,y,w,h){
        super(x,y,w,h);
        this.colideSide = [];
    }

    show(){
        noFill();
        stroke("#333333");
        rect(this.x, this.y, this.w, this.h);
        fill(153,76,0);
        stroke(204,102,0);
        triangle(this.x, this.y, this.x + this.w, this.y, this.x + this.w / 2, this.y + this.h);
    }

}